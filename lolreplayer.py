import asyncio
import re
from typing import Tuple

import lcu_driver

connector = lcu_driver.Connector()
_PATCH_VERSION_RX = re.compile(r"[\d]{2}.[\d]{1,2}")
_MATCH_ID_RX = re.compile(r"[\d]{10}")
CURRENT_PATCH_INFO = {}


def _strip_match_id_from_url() -> int:
    url = input(
        "Please enter the LeagueOfGraphs URL of the match you'd like to watch: "
    )
    while True:
        match_id = _MATCH_ID_RX.search(url)
        if match_id is not None:
            break
        url = input(
            "The URL could not be recognised. Please enter a valid LeagueOfGraphs URL of the match you'd like to watch: "
        )
    return match_id.group(0)


async def _set_patch_version(connection: lcu_driver.connector.Connection) -> str:
    global CURRENT_PATCH_INFO
    req = await connection.request("GET", f"/lol-patch/v1/game-version")
    resp = await req.text()
    _version = _PATCH_VERSION_RX.search(resp)
    if _version is not None:
        version_text = str(_version.group(0))
        CURRENT_PATCH_INFO["number"] = int(version_text.replace(".", ""))
        CURRENT_PATCH_INFO["text"] = version_text
    return CURRENT_PATCH_INFO.get("text")


async def _validate_patch(connection: lcu_driver.connector.Connection, match_id: int) -> Tuple[str, bool]:
    req = await connection.request("GET", f"/lol-match-history/v1/games/{match_id}")
    resp = await req.json()
    raw_patch_value = resp.get("gameVersion")
    full_patch = _PATCH_VERSION_RX.search(raw_patch_value)
    if full_patch is not None:
        patch_played = full_patch.group(0)
    return patch_played, int(patch_played.replace(".", "")) == CURRENT_PATCH_INFO["number"]


async def fetch_replay(connection: lcu_driver.connector.Connection) -> None:
    match_id = _strip_match_id_from_url()
    replay_patch, replay_viewable = await _validate_patch(connection, match_id)
    if not replay_viewable:
        print(
            f"This replay cannot be viewed, as it is not on the most current patch. Replay patch: {replay_patch}, Current Patch: {CURRENT_PATCH_INFO['text']}"
        )
        return
    for op in ["download", "metadata", "watch"]:
        if op == "metadata":
            while True:
                req = await connection.request("GET", f"/lol-replays/v1/{op}/{match_id}")
                resp = await req.json()
                state = resp.get("state")
                if state == "watch":
                    break
                elif state == "lost":
                    print(
                        "The replay could not be downloaded. You did not play in this match, and for some reason Rito Gaming's spaghetti code will not let you download other people's replays..."
                    )
                    return
                await asyncio.sleep(1)
        else:
            if op == "download":
                print("Your replay is downloading. Please wait...")
            else:
                print("Your replay will launch momentarily")
            await connection.request(
                "POST", f"/lol-replays/v1/rofls/{match_id}/{op}", data={"componentType": "string"}
            )
    return


@connector.ready
async def connect(connection: lcu_driver.connector.Connection) -> None:
    patch_version = await _set_patch_version(connection)
    print(f"League Client detected! You are on patch {patch_version}")
    while True:
        await fetch_replay(connection)
        watch_another = input(
            "Would you like to watch another replay? Enter \"Y\" to watch another or enter any other key to quit: "
        ).upper()
        if watch_another != "Y":
            break
    return


if __name__ == "__main__":
    print("Waiting for League Client to open...")
    connector.start()
