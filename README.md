# League Replayer
League Replayer is a program designed to allow playback of League of Legends replays without using the League Client's match history. The client's match history only displays up to the last 20 games, including modes where replays cannot be downloaded. This quickly clogs your match history and can sometimes prevent you from downloading replays from very recent games.

## Running League Replayer
Please check the [releases](https://gitlab.com/marwynnsomridhivej/lol-replayer/-/releases) for Windows and MacOS executables. Alternatively, you may clone this repo, install the required dependencies, and run the [lolreplayer.py](lolreplayer.py) file.

## Usage
League Replayer searches for an open instance of the League Client. When it detects an open instance, you will be able to enter a [LeagueOfGraphs](https://www.leagueofgraphs.com) match summary URL of the match you would like to replay. League Replayer will automatically open the replay once all the corresponding data finishes downloading.

*Please note that only replays of games played in the most up-to-date patch can be successfully played.*

## Contact
To give feedback or report bugs, please contact me through my Discord: **MS Arranges#3060**

## Legal Stuff
League Replayer isn't endorsed by Riot Games and doesn't reflect the views or opinions of Riot Games or anyone officially involved in producing or managing Riot Games properties. Riot Games, and all associated properties are trademarks or registered trademarks of Riot Games, Inc.